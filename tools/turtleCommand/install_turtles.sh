#!/bin/bash

#install pssh
sudo apt install pssh -y

#make executable and move to bin path
chmod +x ./turtles
mkdir ~/.local/bin
install ./turtles ~/.local/bin/turtles

#move turtle_init and make hidden
cp turtle_init .turtle_init
mv .turtle_init ~

#create hosts file for pssh
mkdir ~/.pssh
cp hosts ~/.pssh

#add turtle_init to bashrc and source
echo " " >> ~/.bashrc
echo "#Used for TurtleCommand" >> ~/.bashrc
echo "source ~/.turtle_init" >> ~/.bashrc
source ~/.bashrc
source ~/.turtle_init

echo -e "${LIGHTGREEN}TurtleCommand successfully installed${NC}"
echo " "

