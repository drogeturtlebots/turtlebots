<<<<<<< HEAD
# turtlebots

Main repository for students working with Dr. Droge and turtlebots

To install ROS on your remote PC (laptop) following the tutorial found at http://wiki.ros.org/kinetic/Installation/Ubuntu
Alternatively, you can download the "ros-install.sh" file and run that on your machine.
After you download it, run 

```bash
sudo chmod +x ros-install.sh
./ros-install.sh
```

After ROS is installed, start working through the ROS tutorials found at http://wiki.ros.org/ROS/Tutorials
The Textbook included with the Robotis turtlebots can also be downloaded as a pdf from https://community.robotsource.org/t/download-the-ros-robot-programming-book-for-free/51
=======
